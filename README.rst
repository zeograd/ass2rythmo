Ass2Rythmo
==========

ass2rythmo is a converter which generate rythmo band out of subtitles in ASS (`Advanced SubStation <https://en.wikipedia.org/wiki/SubStation_Alpha#Advanced_SubStation_Alpha>`_) format.

Rythmo bands are used to provide synchronized script for voice actors when dubbing a video.

The text scrolls at a constant speed but has a variable width to adapt to the speech flow that needs to be recorded to be synchronised with lips on screen.

Installation
------------

Ass2Rythmo can be installed from pip::

	pip install ass2rythmo


Alternatively, Ass2Rythmo can be run directly from sources after a git pull::

	git clone https://gitlab.com/zeograd/ass2rythmo.git
	cd ass2rythmo && python setup.py install

Usage
-----

First, generate a timed ASS subtitle file including only the part having to be said by actors.

Then, generate the rythmo band as as collection of transparent png files::

	ass2rythmo my_video_script.ass -R 30

or as a *transparent* movie if you have ffmpeg_ available::

    ass2rythmo my_video_script.ass -R 30 -r rythmo_band.mov

or as *opaque* movie, still with ffmpeg_ available::

    ass2rythmo my_video_script.ass -R 30 -r rythmo_band.mp4

Don't forget to specify the frame rate of the video on which the png will be superposed to ensure a correct synchronisation.

By default, this will generate png file in the current directory named 00000.png, 00001.png, etc...
You can change the directory in which the png files are generated by providing a pattern to the **-r** argument including percent sign
(**/tmp/%05d.png** for instance, for generating png in the **/tmp** directory and padding the frame number on 5 digits.)

If you specify a pattern without any percent sign, it will be considered as a
single movie file output filename.
The result will be encoded with **FFV1** codec and **mov** container if the extension
of the output file is **mov**.

If you use another extension, the default ffmpeg rule will apply. Typically you
may want a **h264** encoded movie in a **mp4** container by asking for an output
filename ending in **mp4**.


Then you can burn the png sequence on top of your movie using your favorite video editor. You can also use **ffmpeg** to perform a quick overlaying, with a command similar to::

	ffmpeg -i input.mp4 -framerate 30 -i %05d.png -filter_complex "overlay=:main_h-overlay_h" output.mp4

to do the same with a transparent mov rythmo band, use something like::

    ffmpeg -i input.mp4 -i rythmo_band.mov -filter_complex "overlay=:main_h-overlay_h" output.mp4


Script creation
---------------

Use any ASS compatible tool, like Aegisub_.

You can use the regular { } to include comments or alternatively [ ] to include content which won't appear in the rythmo band (but still be present if you burn the ass file onto a video, like for actor direction hints).

The bundled font was chosen so that you can include unicode character to show which kind of intonation change you'd like (using unicode arrows).

You can fine tune a single sentence timing using the karaoke tags. You don't have to split your script for the sake of introducing a pause or changing the pace between 2 parts of a sentence.
In this case, you can preview the speed at which every part of your sentence will be rendered using **/kf** instead of the default **/k** tags (both will be accepted by **ass2rythmo**, **/ko** is also working)

Obviously, take care of synchronizing carefully your subtitles with the lips on screen to ensure that recording according to the rythmo band will be synchronized with the images.

You can disable rendering rythmo band for an *actor* by including **[norythmo]** in its name.

CLI
---

The CLI **ass2rythmo** includes sensible default values.
You should be careful about setting the correct frame rate with **-R** option.

You may want to alter the generated width to use fully your available room with **-W** and also change the scroll speed with **-D**.

Other options are less often useful and documented below.


	usage: ass2rythmo [-h] [-d] [-v] [-W TARGET_WIDTH] [-H TARGET_HEIGHT]
	                  [-R TARGET_FPS] [-F FONT_FILENAME] [-D SCROLL_DURATION]
	                  [-r RESULT_FILENAME_FORMAT] [-s SYNC_POINT_RATIO]
	                  ass_filename

	GPL v3+ 2018 Olivier Jolly

	positional arguments:
	  ass_filename          input file as Ass format

	optional arguments:
	  -h, --help            show this help message and exit
	  -d, --debug           print debugging info [default: False]
	  -v, --version         show program's version number and exit
	  -W TARGET_WIDTH, --target_width TARGET_WIDTH
	                        Width of the generated png files [default: 1920]
	  -H TARGET_HEIGHT, --target_height TARGET_HEIGHT
	                        Height of the generated png files. Note that it's not
	                        meant to support low heights at this time [default:
	                        380]
	  -R TARGET_FPS, --target_fps TARGET_FPS
	                        Frame per second to generate. Make sure it matches
	                        your subbed video ! [default: 30]
	  -F FONT_FILENAME, --font_face FONT_FILENAME
	                        Filename of the font used to generate text [default:
	                        [install path]/DejaVuSans.ttf]
	  -D SCROLL_DURATION, --scroll_duration SCROLL_DURATION
	                        Duration of width traversal in ms [default: 4000]
	  -r RESULT_FILENAME_FORMAT, --result_filename_format RESULT_FILENAME_FORMAT
                        Filename template for generated files. May include
                        directory part but won't create any. Including % in
                        the format will generate a list of png files else will
                        generate a transparent movie with the help of ffmpeg
                        [default: %05d.png]
	  -s SYNC_POINT_RATIO, --sync_point_ratio SYNC_POINT_RATIO
	                        Position of the sync point on screen, 0 is leftmost, 1
	                        is rightmost [default: 0.15]

	Convert ASS subtitles to rythmo band as list of transparent png or single transparent movie

LIMITATIONS
-----------

This program is quite young and not extensively tested. Notably, it was only run under linux so far, but no OS specificity were used.

The rendering is done on 2 lines maximum. So with 3 actors speaking at the same time, there will be some overlayed text above each other.

Some artefacts may appear on the leftmost part of short text chunk for some characters (like a "I" to be pronounced very quickly at the beginning of a text chunk).

LICENSE
-------

The ass2rythmo code itself is licensed under GPL V3+ and the bundled DejaVu font is licensed under its own free License (cf LICENCE-dejavu.txt).

.. _Aegisub:  http://aegisub.org
.. _Ffmpeg: http://ffmpeg.org