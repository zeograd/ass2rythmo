# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2023-09-11

### Added

- Modern build support
- Support for subtitle file with incorrect timecode (start > end)
- Opaque movie generation (any non-".mov" file output)

### Fixed

- Overflow bug leading to crash on some text
- Default font location on packaging
- Crash on '/' rendering

## [0.9.1] - 2018-12-17

### Added

- Initial public version

[unreleased]: https://gitlab.com/zeograd/ass2rythmo

[1.0.0]: https://gitlab.com/zeograd/ass2rythmo/-/tags/v1.0.0

[0.9.1]: https://gitlab.com/zeograd/ass2rythmo/-/tags/RELEASE_0_9_1
